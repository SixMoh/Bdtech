/*

Cartouche à remplir
nom du fichier : main.js
date : 23/11/2020
auteur : Eric MARTIN

Framework :  genre symfony

*/

// Variables utilisées
var comboPays = document.FormPays.ListePays;                            //combo Pays dans HTML idem document.getElementById("maListeDesPays");
var comboVille = document.FormPays.ListeVille;                          //combo Ville dans HTML
var paysSelection = document.getElementById("maListeDesPays");
var villeSelection = document.getElementById("maListeDesVilles");
var monAffichage = document.getElementById("pouraffichage");
var saisiePays = document.getElementById("saisieNouveauPays");
var btnSaisiePays = document.getElementById("btnNouveauPays");
var idPaysSelectionne = 0;
var lePaysSelectionner = "";

// 1 - fonction appelé pour remplir ma combbox Pays
function remplissageComboPays(value) {
    comboPays.length++;
    comboPays.options[comboPays.length - 1].text = value.NomPays;
}

// 2 - appelé par getPays pour remplir la combo Ville avec les villes du Pays
function remplissageComboVille(value) {
    var numeroPays = value.idPays;                  // stocke la key du pays

    if (numeroPays == idPaysSelectionne) {
        comboVille.length++;
        comboVille.options[comboVille.length - 1].text = value.NomVille;
    }

}

// 2 appelé par getPays correspondance entre lePaysSelectionner et son Key (Id) dans Pays
function RechercheIdPaysSelectionne(value, key) {  
    if (value.NomPays == lePaysSelectionner) { 
        idPaysSelectionne = key;
    }
}

// 2 permet de récupérer la clef du pays sélectionné et appelle RechercheIdPaysSelectionne
function getPays(element) {
    monAffichage.innerHTML += "Pays = " + element.value;
    lePaysSelectionner = element.value;                     // vaut France, Allemagne ou ....
    pays.forEach(RechercheIdPaysSelectionne);               // pour mettre à jour Key = 1, 2 ou ....
    comboVille.length = 1;
    ville.forEach(remplissageComboVille);                   // on ne met dans Combo Ville que les villes du pays
}

// 3 - appelé à la sélection d'une ville
function getVille(element) {
    monAffichage.innerHTML += " et Ville = " + element.value;
}

// 4 - appelé au click sur le bouton ajoutPays
var ajoutPays = function () {
    // "100" sera la même valeur pour tous les éléments rajoutés !
    // dans une VRAIE base de données, cette key est auto-incrémentée
    pays.set("100", {NomPays: saisiePays.value, NbHabitantsPays: "inconnu"}); // ajoute un nouveau pays dans la Map
    comboPays.length++;                                                         // on le rajoute à la combo
    comboPays.options[comboPays.length - 1].text = saisiePays.value;
}

// 1 remplir la combo des pays
pays.forEach(remplissageComboPays);

// 2 récupérer le Pays sélectionné
paysSelection.addEventListener("change", function () {
    monAffichage.innerHTML = "Votre choix : ";              // pour que les différents choix n'apparaissent pas à la suite des autres
    getPays(this)                                           // pour récupérer la Key du Pays
});

// 3 - récupérer la ville sélectionnée
villeSelection.addEventListener("change", function () {
    getVille(this)
});

// 4 - pour rajouter un pays : on clique sur btnSaisiePays et on exécute ajoutPays
btnSaisiePays.addEventListener("click",ajoutPays, false);